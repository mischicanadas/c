//g++ alpr_imagen.cpp -o alpr_imagen `mysql_config --cflags --libs`
#include <sys/stat.h>
#include <unistd.h>

#include <dirent.h>
#include <iostream>
#include <sstream>
#include <time.h>
#include <vector>

#include <stdexcept>
#include <stdio.h>
#include <string>
#include <functional>

#include <map>
#include <string.h>

#include <algorithm>  
#include <cctype>
#include <locale>

#include <my_global.h>
#include <mysql.h>

using namespace std;

//Variables Globales
string Folder_Procesar = "/var/spool/placas/procesar/";
string Folder_Procesadas = "/var/spool/placas/procesadas/";

//Accesos a BD
string Servidor = "localhost";
string Usuario = "root";
string Password = "V88Tig1";
string BD = "alpr01";

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

//MySQL
void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, (Servidor).c_str(),(Usuario).c_str(),(Password).c_str(),(BD).c_str(), 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

void Ejecuta_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);


    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, (Servidor).c_str(),(Usuario).c_str(),(Password).c_str(),(BD).c_str(), 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    mysql_close(con);
}

int getTotal(string Placa)
{
    int ResTotal = 0;
    string Query = ("SELECT COUNT(iPlaca)AS Total FROM placas WHERE Placa = '")+Placa+"' AND DATE(Fecha) = DATE(NOW()) LIMIT 1";
    MYSQL_RES *result = Regresa_Query((Query).c_str());

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int Total = 0;
    char const *field_id = "Total";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_id, headers[i]) == 0) 
        {
            Total = i;
        }
    }

    while ((row = mysql_fetch_row(result))) 
    {  
        stringstream ss;
        ss << row[Total];
        string Total_s = ss.str();
        ResTotal = atoi((Total_s).c_str());
    }

    mysql_free_result(result);
    return ResTotal;

}

string valida_parecida(string Placa)
{
    string Resultado = "0";

    string Query = ("select iPlaca,comparar_porcentaje('")+Placa+"',Placa) as Parecido,CHAR_LENGTH(Placa)AS Longitud,CHAR_LENGTH('"+Placa+"')AS Longitud_Placa,Fecha from placas HAVING Parecido > 50 AND Fecha >= NOW() - INTERVAL 10 MINUTE order by Longitud DESC LIMIT 1";
    MYSQL_RES *result = Regresa_Query((Query).c_str());

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int iPlaca = 0;
    unsigned int Parecido = 0;
    unsigned int Longitud = 0;
    char const *field_iPlaca = "iPlaca";
    char const *field_Parecido = "Parecido";
    char const *field_Longitud = "Longitud";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_iPlaca, headers[i]) == 0) 
        {
            iPlaca = i;
        }
        if (strcmp(field_Parecido, headers[i]) == 0) 
        {
            Parecido = i;
        }
        if (strcmp(field_Longitud, headers[i]) == 0) 
        {
            Longitud = i;
        }
    }

    stringstream ss_iPlaca;
    stringstream ss_Parecido;
    stringstream ss_Longitud;

    while ((row = mysql_fetch_row(result))) 
    {  
        ss_iPlaca << row[iPlaca];
        ss_Parecido << row[Parecido];
        ss_Longitud << row[Longitud];
    }

    if (strlen(Placa.c_str()) <= atoi((ss_Longitud.str()).c_str()))
    {
        Resultado = ss_iPlaca.str();
    }

    mysql_free_result(result);
    
    return Resultado;
}

//MySQL

//Helpers
// trim del inicio (in place)
static inline void ltrim(string &s) 
{
    s.erase(s.begin(), find_if(s.begin(), s.end(), not1(ptr_fun<int, int>(isspace))));
}

// trim del final (in place)
static inline void rtrim(string &s) 
{
    s.erase(find_if(s.rbegin(), s.rend(), not1(ptr_fun<int, int>(isspace))).base(), s.end());
}

// trim del inicio y del final (in place)
static inline void trim(string &s) 
{
    ltrim(s);
    rtrim(s);
}

// trim del inicio y del final (copying)
static inline string trimmed(string s) 
{
    trim(s);
    return s;
}

string getDate()
{
    time_t t = time(NULL);
	tm* timePtr = localtime(&t);

    stringstream ss_year;
    ss_year << timePtr->tm_year+1900;
    string Year = ss_year.str();

    stringstream ss_month;
    ss_month << timePtr->tm_mon+1;
    string Month = ss_month.str();
    if(atoi(Month.c_str()) < 10)
        Month = "0"+Month;

    stringstream ss_day;
    ss_day << timePtr->tm_mday;
    string Day = ss_day.str();
    if(atoi(Day.c_str()) < 10)
        Day = "0"+Day;

    stringstream ss_hour;
    ss_hour << timePtr->tm_hour;
    string Hour = ss_hour.str();
    if(atoi(Hour.c_str()) < 10)
        Hour = "0"+Hour;

    stringstream ss_min;
    ss_min << timePtr->tm_min;
    string Min = ss_min.str();
    if(atoi(Min.c_str()) < 10)
        Min = "0"+Min;

    stringstream ss_sec;
    ss_sec << timePtr->tm_sec;
    string Sec = ss_sec.str();
    if(atoi(Sec.c_str()) < 10)
        Sec = "0"+Sec;

    string Fecha = Year+Month+Day+"_"+Hour+Min+Sec;

    return Fecha;
}

inline bool archivo_existe (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

void split(const string& s, char c, vector<string>& v) 
{
   string::size_type i = 0;
   string::size_type j = s.find(c);


   while (j != string::npos) {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);


      if (j == string::npos)
         v.push_back(s.substr(i, s.length()));
   }
}

string split_pos(string Texto, char Separador, int Pos)
{
    vector<string> v;
    split(Texto, Separador, v);
    return v[Pos];
}

vector<string> open(string path = ".") 
{
    DIR*    dir;
    dirent* pdir;
    vector<string> files;
    dir = opendir(path.c_str());

    while ((pdir = readdir(dir))!= NULL) {
        files.push_back(pdir->d_name);
    }
    
    return files;
}

string getUID()
{
    //Genero nombre con numero de segundos
    time_t Segundos = time(NULL);
    stringstream ss;
    ss << Segundos;
    string Archivo_UID = ss.str();

    return Archivo_UID;
}

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

int mayor(int a,int b){return((a>b)?(a):(b));}

//Helpers

string procesar(string Camara, string Imagen, string Pais, string Fecha_Hora)
{
    string Placa = "";

    //Ejecuto alpr y obtengo resultado en Res
    string Comando = string("alpr -c ")+ Pais + " " + Imagen + " -n 1 | sed -n 2p | awk '{print $2}'";
    Placa = trimmed(exec(Comando.c_str()));
    string Total = "0";

    if(!Placa.empty())
    {
        //Valido que no este repetida durante el dia actual
        //Total = getTotal(Placa);
        Total = valida_parecida(Placa);
        
        if(Total.compare("0") != 0)
            Placa = "";
    }

    return Placa;
}

int main(int argc,char **argv) 
{
    vector<string> f;
    f = open(Folder_Procesar);

    string Camara = "";
    string Archivo = "";
    string Fecha = "";
    string Hora = "";
    string Fecha_Hora = "";
    string Imagen = "";
    string Imagen_Procesada = "";

    string Placa = "";
    string Placa_US = "";
    string Placa_MX = "";
    string Placa_AU = "";

    unsigned int i = 0;

    for(i = 0; i < f.size(); i++)
    {
        if(f[i].compare(".") != 0 && f[i].compare("..") != 0 )
        {
            //Mensaje(f[i],"Verde");

            //Quito la extension del archivo
            vector<string> Extension;
            split(f[i],'.',Extension);
            //Obtengo la Camara del archivo
            vector<string> Arreglo;
            split(Extension[0], '_', Arreglo);
            Camara = Arreglo[2];
            Fecha = Arreglo[3];
            Hora =  Arreglo[4];
            Fecha_Hora = Fecha+"_"+Hora;
            //Proceso Imagen

            //Obtengo la ruta de la imagen
            Imagen = Folder_Procesar+f[i];

            //Valido si archivo existe
            if (archivo_existe(Imagen))
            {
                //Mensaje(Imagen+ " Si Existe!", "Verde");
                Imagen_Procesada = Folder_Procesadas+f[i];
                string cmd_Mover = string("mv ")+(Imagen).c_str()+" "+(Imagen_Procesada).c_str();
                string cmd_Borrar = string("rm ")+(Imagen).c_str();
                        
                Placa_US = procesar(Camara,Imagen,"us",Fecha_Hora);
                Placa_MX = procesar(Camara,Imagen,"mx",Fecha_Hora);
                Placa_AU = procesar(Camara,Imagen,"au",Fecha_Hora);

                //Busco Placa de mayor longitud
                unsigned int max;
                unsigned long arr[]={strlen(Placa_AU.c_str()),strlen(Placa_MX.c_str()),strlen(Placa_US.c_str())};
                max=mayor(mayor(arr[0],arr[1]),mayor(arr[1],arr[2]));
            
                if(max == strlen(Placa_AU.c_str()))
                    Placa = Placa_AU;
                if(max == strlen(Placa_MX.c_str()))
                    Placa = Placa_MX;
                if(max == strlen(Placa_US.c_str()))
                    Placa = Placa_US;

                if (strlen(Placa.c_str()) == 0)
                {
                    exec(cmd_Borrar.c_str());
                    //Mensaje(cmd_Borrar,"Rojo");
                }
                else
                {
                    exec(cmd_Mover.c_str());

                    //Inserto Placa a Base de datos
                    string Query = string("INSERT INTO placas(Placa,Imagen,iCamara)VALUES('")+Placa+"','"+f[i]+"','"+Camara+"')";
                    Ejecuta_Query((Query).c_str());

                    Mensaje("Placa Insertada: "+Placa,"Azul");
                    //Mensaje(cmd_Mover,"Verde");
                }
                    
            }
            else
                Mensaje(Imagen+ " No Existe!", "Rojo");
            
        }
    }

    return 0;
}