//g++ alpr_camara.cpp -o alpr_camara `mysql_config --cflags --libs`
#include <iostream>
#include <sstream>
#include <time.h>

#include <stdexcept>
#include <stdio.h>
#include <string.h>
#include <string>
#include <functional>

#include <algorithm>  
#include <cctype>
#include <locale>

//MySQL
#include <my_global.h>
#include <mysql.h>

using namespace std;

//Variables Globales
string folder_procesar = "/var/spool/placas/procesar/";

//Accesos a BD
char const *Servidor = "localhost";
char const *Usuario = "root";
char const *Password = "V88Tig1";
char const *BD = "alpr01";

//Mensajes
void Mensaje(string Mensaje, string Color = "Rojo")
{
    if(Color.compare("Negro")==0)
        Color = "30m";
    if(Color.compare("Rojo")==0)
        Color = "31m";
    if(Color.compare("Verde")==0)
        Color = "32m";
    if(Color.compare("Amarillo")==0)
        Color = "33m";
    if(Color.compare("Azul")==0)
        Color = "34m";
    if(Color.compare("Magenta")==0)
        Color = "35m";
    if(Color.compare("Cyan")==0)
        Color = "36m";
    if(Color.compare("Blanco")==0)
        Color = "37m";

    cout << "\033[0;"+Color << Mensaje << " \033[0m\n";
}
//Mensajes

void finish_with_error(MYSQL *con)
{
  fprintf(stderr, "%s\n", mysql_error(con));
  mysql_close(con);
  exit(1);
}

string exec(const char* cmd) 
{
    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try 
    {
        while (!feof(pipe)) 
        {
            if (fgets(buffer, 128, pipe) != NULL)
                result += buffer;
        }
    } 
    catch (...) 
    {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}

string getDate()
{
    time_t t = time(NULL);
    tm* timePtr = localtime(&t);

    stringstream ss_year;
    ss_year << timePtr->tm_year+1900;
    string Year = ss_year.str();

    stringstream ss_month;
    ss_month << timePtr->tm_mon+1;
    string Month = ss_month.str();

    stringstream ss_day;
    ss_day << timePtr->tm_mday;
    string Day = ss_day.str();

    stringstream ss_hour;
    ss_hour << timePtr->tm_hour;
    string Hour = ss_hour.str();

    stringstream ss_min;
    ss_min << timePtr->tm_min;
    string Min = ss_min.str();

    stringstream ss_sec;
    ss_sec << timePtr->tm_sec;
    string Sec = ss_sec.str();

    string Fecha = Year+Month+Day+"_"+Hour+Min+Sec;

    return Fecha;
}

MYSQL_RES *Regresa_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);

    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, Servidor, Usuario, Password, BD, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    MYSQL_RES *result = mysql_store_result(con);
    mysql_close(con);

    if (result == NULL)
    {
        finish_with_error(con);
    }
    return result;
}

void Ejecuta_Query(char const *Query)
{
    MYSQL *con = mysql_init(NULL);


    if (con == NULL)
    {
        fprintf(stderr, "mysql_init() fallo\n");
        exit(1);
    }

    if (mysql_real_connect(con, Servidor, Usuario, Password, BD, 0, NULL, 0) == NULL)
    {
        finish_with_error(con);
    }
    
    if (mysql_query(con, Query))
    {
        finish_with_error(con);
    }

    mysql_close(con);
}

void Procesar(string Camara, string DireccionIP, string Transporte, string Usuario, string Password)
{
    string Comando;
    string TipoTransporte = "";
    string Archivo = "";
    string Archivo_Date = getDate();

    Archivo = folder_procesar+"placas_camara_"+Camara+"_"+Archivo_Date+".jpg";

    if(Transporte.compare("HTTP")==0)
        TipoTransporte = "-i ";
    else
        TipoTransporte = "-rtsp_transport tcp -y -i rtsp://";

    //Formo comando a ejecutar
    if(Usuario.compare("")==0 || Password.compare("")==0)
        Comando = string("ffmpeg ")+TipoTransporte+DireccionIP+" -r 10 -f image2 -frames 1 "+Archivo+" > /dev/null 2>&1";
    else
        Comando = string("ffmpeg ")+TipoTransporte+Usuario+":"+Password+"@"+DireccionIP+":554 -r 10 -f image2 -frames 1 "+Archivo+" > /dev/null 2>&1";

    exec(Comando.c_str());

    Mensaje(Comando,"Verde");
}

int getCamara()
{
    unsigned int ResTotal;
    string Query = "SELECT iCamara,DireccionIP,Transporte,Usuario,Password FROM camaras";
    MYSQL_RES *result = Regresa_Query((Query).c_str());

    int num_fields = mysql_num_fields(result);

    MYSQL_ROW row;
    MYSQL_FIELD *field;
    unsigned int Camara;
    unsigned int DireccionIP;
    unsigned int Transporte;
    unsigned int Usuario;
    unsigned int Password;

    char const *field_Camara = "iCamara";
    char const *field_DireccionIP = "DireccionIP";
    char const *field_Transporte = "Transporte";
    char const *field_Usuario = "Usuario";
    char const *field_Password = "Password";

    char *headers[num_fields];
    for(unsigned int i = 0; (field = mysql_fetch_field(result)); i++) 
    {
        headers[i] = field->name;
        if (strcmp(field_Camara, headers[i]) == 0) 
        {
            Camara = i;
        }
        if (strcmp(field_DireccionIP, headers[i]) == 0) 
        {
            DireccionIP = i;
        }
        if (strcmp(field_Transporte, headers[i]) == 0) 
        {
            Transporte = i;
        }
        if (strcmp(field_Usuario, headers[i]) == 0) 
        {
            Usuario = i;
        }
        if (strcmp(field_Password, headers[i]) == 0) 
        {
            Password = i;
        }
    }

    stringstream ss_Camara;
    stringstream ss_DireccionIP;
    stringstream ss_Transporte;
    stringstream ss_Usuario;
    stringstream ss_Password;

    while ((row = mysql_fetch_row(result))) 
    {  
        ss_Camara << row[Camara];
        ss_DireccionIP << row[DireccionIP];
        ss_Transporte << row[Transporte];
        ss_Usuario << row[Usuario];
        ss_Password << row[Password];

        //Procesar Imagen de Camara
        Procesar(ss_Camara.str(),ss_DireccionIP.str(),ss_Transporte.str(),ss_Usuario.str(),ss_Password.str());

        ss_Camara.str(string());
        ss_DireccionIP.str(string());
        ss_Transporte.str(string());
        ss_Usuario.str(string());
        ss_Password.str(string());
    }

    mysql_free_result(result);

    return 0;
}

int main(int argc,char **argv)
{
    string Archivo = "";
    string Archivo_Date = getDate();
    string Imagen = "";

    //Inicio captura Camara 1
    Mensaje("\n====================== Inicio ======================","Azul");
    getCamara();
   
}