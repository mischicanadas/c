# Deteccion de placas
## Ejemplos para el manejo de imagenes y deteccion de placas

* Conexion a Camaras
* Conexion a MySQL
* Deteccion de placas

## Sistema operativo Ubuntu Budgie 17.04

Descargar ISO de la siguiente direccion [Ubuntu Budgie 17.04](https://ubuntubudgie.org/downloads "Ubuntu Budgie 17.04")

* Usuario de sistema
```
alpr
```

* Password de sistema
```
V88Tig1
```

## Librerias a instalar

* Instalar librerias generales para compilar
```
sudo apt-get install build-essential
sudo apt-get update
```

* Instalar servidor MySQL con password: V88Tig1
```
sudo apt-get install mysql-server
sudo apt-get install libmysqlclient-dev
```
* Instalar servidor APACHE
```
sudo apt-get install apache2

//Agregar al final del archivo: ServerName localhost
sudo nano /etc/apache2/apache2.conf

//Probar configuracion
sudo apache2ctl configtest

//Reiniciar servicio
sudo systemctl restart apache2

//Cambiar Firewall para permitir trafico
sudo ufw allow in "Apache Full"
```

* Instalar PHP
```
sudo apt-get install php libapache2-mod-php php-mcrypt php-mysql

//Agregar prioridad php al servidor web
sudo nano /etc/apache2/mods-enabled/dir.conf

//Ejemplo
<IfModule mod_dir.c>
        DirectoryIndex index.html index.php index.xhtml index.htm
</IfModule>

//Reiniciar Apache
sudo systemctl restart apache2
```

* Instalar openalpr
```
sudo apt-get install openalpr
```

* Crear carpetas para procesar imagenes capturadas
```
sudo mkdir /var/spool/placas
sudo mkdir /var/spool/placas/procesar
sudo mkdir /var/spool/placas/procesadas
```

## Compilar codigo

* Conexion a camaras y captura de imagenes
```
g++ alpr_camara.cpp -o alpr_camara `mysql_config --cflags --libs`
```
Se conecta a la base de datos y obtiene las direcciones IP de las camaras a las que se va a conectar captura imagenes y las guarda en **/var/spool/placas/procesar**

* Procesa las imagenes capturadas para la deteccion de placas
```
g++ alpr_imagen.cpp -o alpr_imagen `mysql_config --cflags --libs`
```
De la carpeta de imagenes **/var/spool/placas/procesar** tomara todas las imagenes y las procesara para buscar placas, si encuentra una placa tomara esa imagen y la movera a la carpeta **/var/spool/placas/procesadas** e insertara un registro en la Base de Datos

## Configurar crontab

* Mover codigo compilado a carpeta de ejecutables
```
mv alpr_camara /usr/local/bin
mv alpr_imagen /usr/local/bin
```

* Editar crontab
```
crontab -e
```
Dentro del editor al final del archivo agregar las siguientes lineas
```
* * * * * /usr/local/bin/alpr_camara
* * * * * /usr/local/bin/alpr_imagen
```

## Descripcion de proceso

* Conexion a las camaras
```
Cada minuto el sistema se conectara a las camaras dadas de alta en la Base de datos  
y obtendra un snapshot de ese momento, es decir habra una imagen nueva cada minuto.
Estas imagenes se guardaran en una carpeta a la espera de ser procesadas.
```

* Deteccion de placas
```
Cada minuto el sistema revisara la carpeta de imagenes para procesarlas una por una  
en busca de placas, si se encuentra una placa se guardara la informacion en la Base de datos  
y la imagen con la placa se movera a otra carpeta, si no se detecta placa en la imagen esta se eliminara.
```